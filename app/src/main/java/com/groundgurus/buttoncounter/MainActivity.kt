package com.groundgurus.buttoncounter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    var counter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val counterButton = findViewById<Button>(R.id.counterButton)
        val counterTextView = findViewById<TextView>(R.id.counterTextView)
        val resetButton = findViewById<Button>(R.id.resetButton)

        counterButton.setOnClickListener {
            counter++
            counterTextView.text = counter.toString()
        }

        resetButton.setOnClickListener {
            counter = 0
            counterTextView.text = counter.toString()
        }
    }
}
